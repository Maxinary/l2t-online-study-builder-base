#!/usr/bin/env python3
# developed in python3.7.4
#
# Max Scribner May 25, 2020

# this is used to see arguments
import sys

# these are used to manipulate files
import os
from pathlib import Path
from shutil import copyfile

import atlasser

'''
    This file builds a website's HTML and JS from a set of templates.
'''

'''
    This code builds the html code (and embedded js) from a set of
      page templates defined in the page_templates directory.
'''
def build_html(templates):
    # Read base HTML file into string
    base_html = ""
    with open("base_template.html") as file:
        base_html = file.read()

    # Find insertion point in the base HTML file
    base_halves = base_html.split("!! UNCOMPILED !! SPLIT HERE !!");

    # For each template to include, add its code blocks to the lists
    html_blocks = []
    js_blocks = []

    for template_name in templates:
        # Read HTML page
        with open("page_templates/%s/%s.html"%(template_name, template_name), 'r') as page:
            print("Reading")
            html_blocks.append(page.read());

        # Read JS code
        with open("page_templates/%s/%s.js"%(template_name, template_name), 'r') as page:
            print("Reading %s"%("page_templates/%s/%s.js"%(template_name, template_name)))
            js_blocks.append(page.read());

    print("\n".join(js_blocks))

    output = ""
    output += base_halves[0]
    output += "\n".join(html_blocks)
    output += base_halves[1]
    output += "<script>"
    output += "\n".join(js_blocks)
    output += "</script>"
    output += base_halves[2]

    return output

# return a list of first word on each line
def parse_buildfile(file):
    lis = []
    with open(file, "r") as f:
        for line in f: # note this could be more efficient, but buildfiles are
                       #  not very long, so no pressing need to optimize
            name = line.strip()
            if name != "":
                lis.append(name) # I use strip to remove \n's
    return lis

# the first parameter is the name, all others are the templates to include
# if a buildfile exists in builds/buildfiles, do ur thing
if __name__ == "__main__":
    folder = "build/"

    Path(folder + "run.js").touch()
    Path(folder + "custom.css").touch()

    copyfile("pages.js", folder+"pages.js")

    HTML = ""
    buildfile = "templates.txt"
    template_list = parse_buildfile(buildfile)

    HTML = build_html(template_list)

    with open(folder+"index.html", "w") as f:
        f.write(HTML)

    atlasser.write_atlas_dir()
