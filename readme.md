# Web Experiment Builder

This is the codebase for the Learning to Talk Lab's 2020-2021 web-based experiments. This software allows for a programmer to assemble one website from a group of sequential pages. These pages are instances of general page templates which the programmer defines beforehand (this comes in handy when many pages share the same general functionality).

This site uses JQuery 3.4.1

## Page Templates
A page template is a pair of HTML and JavaScript files which correspond to one type of page. A page template fully defines what happens (1) when a page of its type is created, (2) when it is shown to the user, and (3) when it the user completes that page. Page templates are located in the `page_templates/` folder, and are in the format
```
page_templates/
  ...
  template_name/
    template_name.html
    template_name.js
  ...
```

The `build_html.py` file takes the templates specified in `templates.txt`, and packages them into one html file: `buildfiles/index.html`.

A template's html file contains style, and a top-level `div class="page"` tag. Page class `div` elements are globally styled to fill up the entire screen. Within each page is the body of the given page which will be shown to the user. For example:
```
<style>
  #question-page > div {
      display: flex;
      justify-content: center;
      width:100%;
      margin:15px;
  }
</style>

<div class="page" id="question-page">
  <div>
    <h3 style="color:white;"></h3>
  </div>
  <div>
    <input id="q-in" style="width:240px;"> </input><br />
  </div>
  <div>
    <input class="exit" type="button" value="Next"></input>
  </div>
</div>
```

In the template's JavaScript, the programmer specifies a Page class associated with the template. This programmatically manages the page, and is instantiated used in `build/run.js` to order pages:
```
class QuestionPage extends Page {
  constructor(pageID, question) {
    super(pageID, "question");

    this.q = question;
  }

  play() {
    $("#question-page > div > h3").html(this.q);

    $("#q-in")[0].value = "";

    $("#question-page").show();
  }

  deactivate() {
    new_datum(this.pageID, this.q, $("#q-in")[0].value);

    $("#question-page").hide();
  }
}
```

### Pages, Books
Each page class has three necessary functions within it: `constructor`, `play`, and `deactivate`. `constructor` stores any relevant parameters of the page. `play` is called when a page is to be displayed to the site user. It updates the content in the div associated with the page's template, and shows that div. `deactivate` clears the page, logs any output, and hides it.

These are all called by the Book, which is an ordered list of pages. The book also contains code to handle the movement between pages. A book is manually assembled and started by the programmer in `build/run.js`.

When switching between pages, the book will
1. Run the current page's `deactivate` method
2. Increment the internal page number
3. Run the next page's `play` method

There is one global book (in JavaScript `globalBook`), which is active for the entirety of the experiment. To start the experiment, run the function `run_book(...)` with a locally created book as its sole parameter. The local page is created by defining a JavaScript list of pages (each instantiations of page template classes), and passing that into the `new Book(...)` constructor (defined in `pages.js`).

### Data Logging
Data logging must be defined by the programmer, as it will change for each project. The way Learning to Talk did this was to continuously add new rows to a global list of data (defined inside a `page_template/` Javascript file with no Page class), and on the last page to send the data to the server via an Ajaj post request (via JQuery's Ajax function).

## Audio
The Learning to Talk lab spent a great deal of time optimizing the audio to run on all devices. The final set-up was to generate an audio "atlas" from all of the audio files necessary for the experiment. This atlas is paired with a JavaScript object detailing where and how long each audio file is within the atlas. The atlas is generated automatically when `build_html.py` is run, by the `atlasser.py` library. It takes audio from the `buildfiles/` directory. The handling of that audio atlas is inside of `page_templates/audio/audio.js`. To play an audio file, run `play_audio(filename)`.

## Bringing It All Together
To assemble the page templates into the site, run the command `python3 build_html.py`. Once this is complete, to run the site open `build/index.html` in your browser.

Internally, when the site is loaded in your browser, the site goes through these steps
1. Load all the page templates
2. Load the audio file
3. execute the code in `run.js`

`run.js` sets up the book, and, once it is done, it opens the first page by running `run_book(...)`. That page must define a condition for moving to the next page on its own (whether that's pressing a button, or a fixed amount of time, etc.). It must call `globalBook.nextpage()` to move to the next page.
