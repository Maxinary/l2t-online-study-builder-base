class DocPage extends Page {
  constructor(pid, text, width_str, fontsize, buttontext) {
    super(pid, 400);

    this.text = text;
    this.width = width_str;
    this.fontsize = fontsize;

    this.buttontext = buttontext;
  }


  play() {
    super.play();
    $("#doc-page").css("--form-width", this.width)
    $("#doc-page > #form-page-center").html(this.text)
    $("#doc-page > #form-page-center").css("font-size", this.fontsize)

    $("#doc-page > div > input ").attr("value", this.buttontext)

//    $("#doc-page > div > input").attr("disabled", "disabled")

    $("#doc-page").show();
  }

  deactivate() {
    super.deactivate();

    $("#doc-page").hide();
  }
}
