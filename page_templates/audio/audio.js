class AudioAtlas {
  constructor(atlasObject) {
      this.file = atlasObject.file;
      this.locations = atlasObject.locations;

      this.audioend = 0;
      this.pausing = false;
      this.afterfunction = function(){};

      this.audio = new Audio(this.file);
      this.audio.addEventListener("canplaythrough", function() {
        if (!this.loaded) {
          this.loaded = true;
          this.audio.pause();
          console.log("canplaythrough");

          audio_loaded.i = true;
        }
      }.bind(this));

      this.audio.addEventListener('timeupdate', function() {
        if (this.audio.currentTime >= this.audioend) {
          if (!this.pausing) {
            this.audio.pause();
            this.pausing = true;
            console.log("pausing");
            this.afterfunction(); // double check that this is only run once
          }
        }
      }.bind(this), false);

      this.audio.addEventListener('stalled', function() {
        if (this.audio.currentTime >= this.audioend) {
          alert("Audio is not loading. Please check internet connection. Reloading now.");

          this.audio.load();
          this.audio.pause();
        }
      }.bind(this), false);

      this.loaded = false;
  }

  playFile(name) {
    if (name in this.locations) {
      this.playTime(this.locations[name].start, this.locations[name].length);
    } else {
      console.log(`FILE ${name} not available in atlas`);
    }
  }

  playTime(start, length) {
    if (this.loaded) {
      this.audio.currentTime = start;

      console.log(`Start: ${start}\nEnd:${start+length}`);

      this.audioend = start+length;
      this.audio.play();
      this.pausing = false;
    } else {
      console.log("CANNOT play audio because it is unloaded.");
    }
  }

  initialize() {
    if (!this.loaded) {
      this.audio.play();
    }
  }

  paused() {
    return this.audio.paused;
  }

  pause() {
    this.audioend = 0;
  }
}

_global_audio = undefined;
$(window).on("load", function() {
  _global_audio = new AudioAtlas(_global_audio_object);
});

$("input").bind("click touch", function() {
  _global_audio.initialize();
});


function play_audio(file, afterfunction) {
  _global_audio.pause();
  if (file == undefined) {
    console.log("NO LOAD");

    afterfunction();
  } else {
    _global_audio.afterfunction = afterfunction;

    _global_audio.playFile(file);
    }
}

class AudioPage extends Page {
  constructor(pageID, audio, delay) {
    super(pageID);

    this.audiofile = audio;
    this.delay = delay;
  }

  after_audio() {}

  play_audio() {
    play_audio(this.audiofile, function() {
        (this.after_audio.bind(this))();
    }.bind(this));
  }

  play() {
    this.setTimeout(this.play_audio.bind(this), this.delay);
  }

  deactivate() {
    super.deactivate();
    _global_audio.pause();
  }
}
