// This is the Fisher-Yates shuffle
// Taken from https://bost.ocks.org/mike/shuffle/
function shuffle(array) {
  var m = array.length, t, i;

  // While there remain elements to shuffle…
  while (m) {

    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = array[m];
    array[m] = array[i];
    array[i] = t;
  }

  return array;
}

// takes two lists of equal length
//  list is anything
//  arrangement is a shuffling of range(0, len(arrangement))
function reorder(list, arrangement) {
  var o = [];

  for(var i=0; i< arrangement.length; i++) {
    o.push(list[arrangement[i]])
  }

  return o;
}
